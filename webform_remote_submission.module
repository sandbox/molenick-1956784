<?php
/**
 * @file
 * Webform Remote Submission module file.
 *
 * Allows remote sites to post to a webform.
 */

/**
 * Implements hook_permission().
 */
function webform_remote_submission_permission() {
  return array(
    'view remote submission code' => array(
      'title' => t('View remote webform submission code'),
      'description' => t('Allows users to view html embed code for remote form submissions.'),
    ),
    'configure remote form submissions' => array(
      'title' => t('Configure remote form submissions'),
      'description' => t('Allows a user to configure remote submissions for a webform.'),
      'restrict access' => TRUE,
      'warning' => t('This will allow external systems to post data to individual webforms'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function webform_remote_submission_menu() {
  $items['node/%node/wrs/post'] = array(
    'page callback' => '_webform_remote_submission_post',
    'page arguments' => array(1),
    'access callback' => '_webform_remote_submission_post_access',
    'access arguments' => array(1),
  );
  // @todo Come up with better names/paths.
  $items['node/%node/wrs/code'] = array(
    'title' => 'Remote embed code',
    'page callback' => '_webform_remote_submission_code',
    'page arguments' => array(1),
    'access callback' => '_webform_remote_submission_code_access',
    'access arguments' => array(1),
    'weight' => 5,
    'type' => MENU_LOCAL_TASK,
  );
  $items['node/%node/wrs/request-form'] = array(
    'page callback' => '_webform_remote_submission_request_form',
    'page arguments' => array(1),
    'access callback' => '_webform_remote_submission_code_access',
    'access arguments' => array(1),
  );
  return $items;
}

/**
 * Implements hook_form_alter().
 */
function webform_remote_submission_form_webform_configure_form_alter(&$form, &$form_state, $form_id) {
  // Only display remote submit settings to authorized users.
  $access = user_access('enable remote form submissions');
  if (!$access) {
    return;
  }
  // Get existing settings for the webform node.
  $node = $form['#node'];
  $remote_allow = _webform_remote_submission_is_remote($node);
  $referer_redirect = _webform_remote_submission_has_referer_redirect($node);
  $form['remote'] = array(
    '#type' => 'fieldset',
    '#title' => t('Remote submission settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => -5,
  );
  $form['remote']['remote_allow'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow remote submissions'),
    '#description' => t('Checking this allows for third-party sites to post submissions to this form.'),
    '#default_value' => $remote_allow,
  );
  $form['remote']['remote_referer_redirect'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show link back to referer on submit'),
    '#description' => t('Checking this sets a message with a link back to the referer on remote submission.'),
    '#default_value' => $referer_redirect,
  );
  $form['#submit'][] = '_webform_remote_submission_configure_submit';
}

/**
 * Implements hook_node_insert().
 */
function webform_remote_submission_node_insert($node) {
  // Default remote posts to "no".
  if (_webform_remote_submission_is_webform($node)) {
    $write = new stdClass();
    $write->nid = $node->nid;
    $write->remote = 0;
    $write->referer_redirect = 0;
    drupal_write_record('webform_remote_submission', $write);
  }
}

/**
 * Implements of webform_remote_submission_node_load().
 */
function webform_remote_submission_node_load($nodes, $types) {
  // Process nodes, checking to see if they're actually a webform.
  foreach ($nodes as $key => $node) {
    if (_webform_remote_submission_is_webform($node)) {
      // If it is a webform, place its remote submission setting in the array.
      $nodes[$key]->webform['remote']['allow'] = _webform_remote_submission_is_remote($node);
      $nodes[$key]->webform['remote']['referer_redirect'] = _webform_remote_submission_has_referer_redirect($node);
    }
  }
}

/**
 * Implements hook_node_delete().
 */
function webform_remote_submission_node_delete($node) {
  // Delete the remote webform record from the database.
  db_delete('webform_remote_submission')
    ->condition('nid', $node->nid)
    ->execute();
}

/**
 * Custom submit function for webform settings.
 */
function _webform_remote_submission_configure_submit(&$form, &$form_state) {
  // Determine if there's already a record for this.
  $node = $form['#node'];
  $write = new stdClass();
  $write->nid = $node->nid;
  // Write allow.
  if ($form_state['values']['remote_allow'] == 1) {
    $write->remote = 1;
  }
  if ($form_state['values']['remote_allow'] == 0) {
    $write->remote = 0;
  }
  // Write redirect.
  if ($form_state['values']['remote_referer_redirect'] == 1) {
    $write->referer_redirect = 1;
  }
  if ($form_state['values']['remote_referer_redirect'] == 0) {
    $write->referer_redirect = 0;
  }

  $remote = _webform_remote_submission_is_remote($node);
  if (is_null($remote)) {
    drupal_write_record('webform_remote_submission', $write);
  }
  else {
    drupal_write_record('webform_remote_submission', $write, 'nid');
  }
}

/**
 * Returns a webform's escaped remotely embeddable html code.
 *
 * Callback for hook_menu().
 *
 * @param object $node
 *   The node to load the webform from.
 *
 * @return string
 *   The escaped html form code.
 */
function _webform_remote_submission_code($node) {
  global $base_url;
  $path = $base_url . '/node/' . $node->nid . '/wrs/request-form';
  $did = preg_replace("/[^a-z0-9]+/i", "", $base_url) . '_wrs_' . $node->nid;
  $script = "
    <script type=\"text/javascript\">
    var bannerDiv = document.createElement('div');
    bannerDiv.setAttribute('id', '{$did}');
    var bodyElems = document.getElementsByTagName('body');
    var body = bodyElems[0];
    body.appendChild(bannerDiv);
    var http = new XMLHttpRequest();
    http.open('GET', '$path', false);
    http.send();
    var serverResponse = http.responseText;
    document.getElementById('$did').innerHTML = serverResponse;
    </script>";
  return check_plain($script);
}

/**
 * Returns a webform's remotely embeddable html code.
 *
 * @param object $node
 *   Rhe node load the webform from.
 *
 * @return string
 *   The html form code.
 */
function _webform_remote_submission_render_form($node) {
  global $base_url;
  $webform = drupal_get_form('webform_client_form_' . $node->nid, $node, (object) array(), TRUE, FALSE);

  // Add custom action.
  $webform['#action'] = $base_url . '/node/' . $node->nid . '/wrs/post';
  $render = drupal_render($webform);
  return $render;
}

/**
 * Receives a remote post to a webform.
 *
 * Callback for hook_menu().
 *
 * @param object $node
 *   The webform node to post against.
 *
 * @return string
 *   Optionally return a string to print to the page, otherwise redirect.
 */
function _webform_remote_submission_post($node) {
  // @todo redo this w/ new remote token system.
  // If remote submissions aren't allowed for this node, return.
  if (!_webform_remote_submission_is_allowed($node)) {
    return t("Remote submissions are not allowed for this @type.", array('@type' => $node->type));
  }

  // If there's no post data, there's no reason to be here.
  if (count($_POST) == 0) {
    return t("There was no submission.");
  }

  // Let keep $_POST intact, just in case.
  $remote_form = $_POST;
  $form_id = $remote_form['form_id'];
  $form_state['values'] = $remote_form;
  drupal_form_submit($form_id, $form_state, $node, (object) array());

  // If option is set, display link back to referer.
  if (_webform_remote_submission_has_referer_redirect($node)) {
    if ($_SERVER['HTTP_REFERER']) {
      $referer = l($_SERVER['HTTP_REFERER'], $_SERVER['HTTP_REFERER']);
      $opts = array(
        '@title' => $node->title,
        '!referer' => $referer,
      );
      drupal_set_message(t('Thanks for submitting @title. Return to !referer.', $opts));
    }
  }

  // If the webform is to to "No redirect - reload current page," then
  // redirect to node/%nid so that it works like vanilla webform.
  // Otherwise it will redirect to node/%nid/wrs/post and say
  // "No submission," which is confusing to end users.
  if ($node->webform['redirect_url'] == '<none>' && !isset($form_state['redirect'])) {
    $form_state['redirect'] = 'node/' . $node->nid;
  }

  // The redirect won't happen if programmed is set, since it could
  // break anything left by a calling script.  There's nothing left for
  // us to do, so this is safe.
  unset($form_state['programmed']);
  drupal_redirect_form($form_state);
}

/**
 * Get a raw HTML copy of the form for remote transport.
 *
 * @param object $node
 *   The webform node to act upon.
 *
 * @return string
 *   A string of raw HTML containing the webform.
 */
function _webform_remote_submission_request_form($node) {
  header('Content-type: text/html');
  header('Access-Control-Allow-Origin: *');
  print _webform_remote_submission_render_form($node);
}

/**
 * Checks to see if remote posting is allowed for a given webform.
 *
 * @param object $node
 *   The webform node to post against.
 *
 * @return bool
 *   True or false depending on whether or not a remote post can happen.
 */
function _webform_remote_submission_is_allowed($node) {
  $access = FALSE;
  // Is it a webform?
  if (_webform_remote_submission_is_webform($node)) {
    $access = TRUE;
  }
  // Is remote submission allowed for this node?
  if (_webform_remote_submission_is_remote($node) == 1) {
    $access = TRUE;
  }
  return $access;
}

/**
 * Checks to see node is webform and user has access.
 *
 * Callback for hook_menu() (access).
 *
 * @param object $node
 *   The webform node to post against.
 *
 * @return bool
 *   True or false depending on whether a user has access.
 */
function _webform_remote_submission_post_access($node) {
  global $user;

  // Check if the user's role can submit this webform.
  if (variable_get('webform_submission_access_control', 1)) {
    foreach ($node->webform['roles'] as $rid) {
      $allowed_roles[$rid] = isset($user->roles[$rid]) ? TRUE : FALSE;
    }
    if (array_search(TRUE, $allowed_roles) === FALSE && $user->uid != 1) {
      return FALSE;
    }
  }
  // Check if the node is a webform.
  if (!_webform_remote_submission_is_webform($node)) {
    return FALSE;
  }
  // Check if the webform has been setup for remote submission.
  if (!_webform_remote_submission_is_remote($node)) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Checks to see if a user has access to the remote webform code.
 *
 * Callback for hook_menu() (access).
 *
 * @param object $node
 *   The webform node to post against.
 *
 * @return bool
 *   True or false depending on whether a user has access.
 */
function _webform_remote_submission_code_access($node) {
  if (!user_access('view remote submission code')) {
    return FALSE;
  }
  if (!_webform_remote_submission_is_webform($node)) {
    return FALSE;
  }
  if (!_webform_remote_submission_is_remote($node)) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Checks to see node is a webform.
 *
 * @param object $node
 *   The webform node to post against.
 *
 * @return bool
 *   True or false depending on whether its a webform or not.
 */
function _webform_remote_submission_is_webform($node) {
  if (!isset($node->nid)) {
    return FALSE;
  }
  if (!isset($node->webform)) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Checks to see if remote posting is enabled.
 *
 * @param object $node
 *   The webform node to post against.
 *
 * @return bool
 *   True or false depending on whether remote posting is enabled.
 */
function _webform_remote_submission_is_remote($node) {
  $remote = NULL;
  if (isset($node->webform['remote']['allow'])) {
    $remote = $node->webform['remote']['allow'];
  }
  else {
    $rs = db_query("SELECT remote FROM {webform_remote_submission} WHERE nid = :nid", array(':nid' => $node->nid));
    foreach ($rs as $record) {
      $remote = $record->remote;
    }
  }
  if ($remote == 1) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Checks to if referer link-back should be enabled.
 *
 * @param object $node
 *   The webform node to post against.
 *
 * @return bool
 *   True or false depending on whether link-back should display.
 */
function _webform_remote_submission_has_referer_redirect($node) {
  $referer_redirect = NULL;
  if (isset($node->webform['remote']['referer_redirect'])) {
    $referer_redirect = $node->webform['remote']['referer_redirect'];
  }
  else {
    $rs = db_query("SELECT referer_redirect FROM {webform_remote_submission} WHERE nid = :nid", array(':nid' => $node->nid));
    foreach ($rs as $record) {
      $referer_redirect = $record->referer_redirect;
    }
  }
  if ($referer_redirect == 1) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}
