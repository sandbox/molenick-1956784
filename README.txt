Description:
Webform Remote Submission allows a site to accept a webform submission 
from a remote site.  It provides some JavaScript code for embedding a
given form on an external site.

Note that this module should used with great care, as any webform you
setup for remote submisison can be submitted from anywhere.

Dependencies:
Drupal 7.x
Webform

Installation:
1) Download and install module as per normal.
2) Configure a content type to be usuable as a webform.
3) Grant permissions for configuring remote posting and viewing form 
   code.
3) In the webform settings page check "Allow remote submissions" to 
   allow remote submissions to that form.
4) Get the remote form code from node/%nid/wrs/code.
5) Embed the JavaScript code on a remote site after the </body> tag.

Permissions:
View remote webform submission code:
--Allows users to view html embed code for remote form submissions.
Configure remote form submissions:
--Allows a user to configure remote submissions for a webform. This will 
  allow external systems to post data to individual webforms

Configuration Options:
Allow Remote Form Submission
--Allows for third-party sites to post submissions to a form.
Show link back to referer on submit
--Optionally show a link back to the referring site.

Known Issues:
Best use cases for redirection are still being worked out. For now, it's
recommended you use the "Confirmation Page" setting.  Please contribute
to additional redirect use cases here: https://drupal.org/node/1994922

Report issues:
https://drupal.org/sandbox/molenick/1956784
